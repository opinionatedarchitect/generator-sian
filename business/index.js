'use strict';
var util = require('util'),
        fs = require('fs'),
        path = require('path'),
        yeoman = require('yeoman-generator'),
        chalk = require('chalk'),
        _s = require('underscore.string'),
        shelljs = require('shelljs'),
        scriptBase = require('../script-base');


var BusinessGenerator = module.exports = function BusinessGenerator(args, options, config) {
    //yeoman.generators.NamedBase.apply(this, arguments);
    yeoman.generators.Base.apply(this, arguments);
    this.useConfigurationFile =false;

    this.baseName = this.config.get('baseName');
    this.packageName = this.config.get('basePackage Name');
    this.packageFolder = this.packageName.replace(/\./g, '/');
    this.javaVersion = this.config.get('javaVersion');
};

util.inherits(BusinessGenerator, yeoman.generators.Base);
util.inherits(BusinessGenerator, scriptBase);

BusinessGenerator.prototype.askForFields = function askForFields() {
    if (this.useConfigurationFile == true) {// don't prompt if data are imported from a file
        return;
    }
    var cb = this.async();
    this.fieldId++;
    console.log(chalk.green('Generating field #' + this.fieldId));
    var prompts = [
       {
            type: 'input',
            name: 'businessName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id'/* && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1 */) return true;
                return 'Your business name cannot contain special characters or use an already existing field name';
            },
           message: 'What is the name of your business object?'
       },
       {
            type: 'input',
            name: 'entityName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id'/* && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1 */) return true;
                return 'Your entity name cannot contain special characters or use an already existing field name';
            },
           message: 'What is the name of your entity object?'
       }
    ];

    
    this.prompt(prompts, function (props) {
        this.businessName = props.businessName;
        this.entityName = props.entityName;
        cb();
    }.bind(this));
};



BusinessGenerator.prototype.files = function files() {
    //if (this.databaseType == "sql") {
    //    this.changelogDate = this.dateFormatForLiquibase();
    //}
    if (this.useConfigurationFile == false) { // store informations in a file for further use.
        this.data = {};
        this.data.relationships = this.relationships;
        this.data.fields = this.fields;
        this.data.fieldNamesUnderscored = this.fieldNamesUnderscored;
        this.data.fieldsContainOwnerManyToMany = this.fieldsContainOwnerManyToMany;
        this.data.fieldsContainOneToMany = this.fieldsContainOneToMany;
        this.data.fieldsContainLocalDate = this.fieldsContainLocalDate;
        this.data.fieldsContainCustomTime = this.fieldsContainCustomTime;
        this.data.fieldsContainBigDecimal = this.fieldsContainBigDecimal;
        this.data.fieldsContainDateTime = this.fieldsContainDateTime;
        this.data.changelogDate = this.changelogDate;
        this.filename = '.sian.' + this.businessName + '.json';
        this.write(this.filename, JSON.stringify(this.data, null, 4));
    } else  {
        this.relationships = this.fileData.relationships;
        this.fields = this.fileData.fields;
        this.fieldNamesUnderscored = this.fileData.fieldNamesUnderscored;
        this.fieldsContainOwnerManyToMany = this.fileData.fieldsContainOwnerManyToMany;
        this.fieldsContainOneToMany = this.fileData.fieldsContainOneToMany;
        this.fieldsContainLocalDate = this.fileData.fieldsContainLocalDate;
        this.fieldsContainCustomTime = this.fileData.fieldsContainCustomTime;
        this.fieldsContainBigDecimal = this.fileData.fieldsContainBigDecimal;
        this.fieldsContainDateTime = this.fileData.fieldsContainDateTime;
        this.changelogDate = this.fileData.changelogDate;
    }
    this.businessClass = _s.capitalize(this.businessName);
    this.businessInstance = this.businessName.charAt(0).toLowerCase() + this.businessName.slice(1);

    this.entityClass = _s.capitalize(this.entityName);
    this.entityInstance = this.entityName.charAt(0).toLowerCase() + this.entityName.slice(1);

    var insight = this.insight();
    insight.track('generator', 'business');
    //insight.track('business/fields', this.fields.length);
    //insight.track('business/relationships', this.relationships.length);

    var resourceDir = 'business/src/main/resources/';

    this.template('business/src/main/java/package/business/_Business.java',
        'business/src/main/java/' + this.packageFolder + '/business/I' +    this.businessClass + 'Business.java', this, {});

    this.template('business/src/main/java/package/business/impl/_BusinessImpl.java',
        'business/src/main/java/' + this.packageFolder + '/business/impl/' +    this.businessClass + 'Business.java', this, {});
  
    this.template('business/src/test/groovy/package/test/business/_BusinessSpec.groovy',
        'business/src/test/groovy/' + this.packageFolder + '/test/business/' +    this.businessClass + 'BusinessSpec.groovy', this, {});  
};

