package <%=packageName%>.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import <%=packageName%>.repository.I<%=entityClass%>Repository; 
import <%=packageName%>.entity.<%=entityClass%>; 
import <%=packageName%>.business.I<%=businessClass%>Business;

@Service
public class <%=businessClass%>Business implements I<%=businessClass%>Business {
	
	@Autowired I<%=entityClass%>Repository <%=entityInstance%>Repository;
	
	@Override
	public <%=entityClass%> save(<%=entityClass%> client) {
		return this.<%=entityInstance%>Repository.save(client);
	}
	
	@Override
	public List<<%=entityClass%>> findAll() {
		return this.<%=entityInstance%>Repository.findAll();
	}
	
	@Override
	public <%=entityClass%> findById(Long id) {
		return this.<%=entityInstance%>Repository.findOne(id);
	}
}
