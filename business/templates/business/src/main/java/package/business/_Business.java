package <%=packageName%>.business; 

import java.util.List;

import <%=packageName%>.entity.<%=entityClass%>; 

public interface I<%=businessClass%>Business  {

	<%=entityClass%> save(<%=entityClass%> client);
	
	List<<%=entityClass%>> findAll();
	
	<%=entityClass%> findById(Long id);
}
