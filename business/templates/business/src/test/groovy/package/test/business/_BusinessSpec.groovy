package <%=packageName%>.test.business

import <%=packageName%>.repository.I<%=entityClass%>Repository; 
import <%=packageName%>.entity.<%=entityClass%>; 
import <%=packageName%>.business.I<%=businessClass%>Business;
import <%=packageName%>.business.impl.<%=businessClass%>Business;


import spock.lang.Specification

class <%=businessClass%>BusinessSpec extends Specification {
	
	I<%=businessClass%>Business <%=businessInstance%>Business;

	def setup() {
		this.<%=businessInstance%>Business = new <%=businessClass%>Business();
	}

	def "simple save"() {
		setup:
			this.<%=businessInstance%>Business.@<%=entityInstance%>Repository = [save:{ [id: 1L] as <%=entityClass%>}] as I<%=entityClass%>Repository;
		
		when:
			def rentity = this.<%=businessInstance%>Business.save(new <%=entityClass%>());
			
		then:
			rentity != null;
			rentity.id == 1L;
	}

	def "empty list"() {
		setup:
			this.<%=businessInstance%>Business.@<%=entityInstance%>Repository = [findAll: {[] as List<<%=entityClass%>>}] as I<%=entityClass%>Repository;
		
		when:
			def rlist = this.<%=businessInstance%>Business.findAll();
		
		then:
			rlist != null;
			rlist.isEmpty();
	}
	
	def "one in list"() {
		setup:
			this.<%=businessInstance%>Business.@<%=entityInstance%>Repository = [findAll: {[[name: "Bobby"] as <%=entityClass%>] as List<<%=entityClass%>>}] as I<%=entityClass%>Repository;
		
		when:
			def rlist = this.<%=businessInstance%>Business.findAll();
		
		then:
			rlist != null;
			rlist.size() == 1;
	}
	
	def "find one by id"() {
		setup:
			this.<%=businessInstance%>Business.@<%=entityInstance%>Repository = [findOne: {[name: "Bobby"] as <%=entityClass%>}] as I<%=entityClass%>Repository;
		
		when:
			def client = this.<%=businessInstance%>Business.findById(1L);
		
		then:
			client != null;
	}
}
