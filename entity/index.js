'use strict';
var util = require('util'),
        fs = require('fs'),
        path = require('path'),
        yeoman = require('yeoman-generator'),
        chalk = require('chalk'),
        _s = require('underscore.string'),
        shelljs = require('shelljs'),
        scriptBase = require('../script-base');


var EntityGenerator = module.exports = function EntityGenerator(args, options, config) {
    //yeoman.generators.NamedBase.apply(this, arguments);
    yeoman.generators.Base.apply(this, arguments);
    this.useConfigurationFile =false;

    this.baseName = this.config.get('baseName');
    this.packageName = this.config.get('basePackage Name');
    this.packageFolder = this.packageName.replace(/\./g, '/');
    this.javaVersion = this.config.get('javaVersion');

    this.fieldId = 0;
    this.fields = [];
    this.fieldsContainLocalDate = false;
    this.fieldsContainDateTime = false;
    this.fieldsContainNotNull = false;
    this.fieldsContainCustomTime = false;
    this.fieldsContainBigDecimal = false;
    this.fieldsContainOwnerManyToMany = false;
    this.fieldsContainOneToMany = false;
    this.relationshipId = 0;
    this.relationships = [];
};

var fieldNamesUnderscored = ['id'];

util.inherits(EntityGenerator, yeoman.generators.Base);
util.inherits(EntityGenerator, scriptBase);

EntityGenerator.prototype.askForEntityName = function askForEntityName() {
    var cb = this.async();
    var prompts = [
       {
            type: 'input',
            name: 'entityName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id' && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1 ) return true;
                return 'Your Entity name cannot contain special characters or use an already existing field name';
            },
           message: 'What is the name of your entityName?'
       }
    ];

    this.prompt(prompts, function (props) {
        this.entityName = props.entityName;
        cb();
    }.bind(this));
}

EntityGenerator.prototype.askForFields = function askForFields() {
    if (this.useConfigurationFile == true) {// don't prompt if data are imported from a file
        return;
    }
    var cb = this.async();
    this.fieldId++;
    console.log(chalk.green('Generating field #' + this.fieldId));
    var prompts = [
        {
            type: 'confirm',
            name: 'fieldAdd',
            message: 'Do you want to add a field to your entity?',
            default: true
        },
        {
            when: function (response) {
                return response.fieldAdd == true;
            },
            type: 'input',
            name: 'fieldName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id' && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1) return true;
                return 'Your field name cannot contain special characters or use an already existing field name';
            },
            message: 'What is the name of your field?'
        },
        {
            when: function (response) {
                return response.fieldAdd == true;
            },
            type: 'confirm',
            name: 'fieldNotNull',
            message: 'Is it not null(default false)?',
            default: false
        },
        {
            when: function (response) {
                return response.fieldAdd == true;
            },
            type: 'list',
            name: 'fieldType',
            message: 'What is the type of your field?',
            choices: [
                {
                    value: 'String',
                    name: 'String'
                },
                {
                    value: 'Integer',
                    name: 'Integer'
                },
                {
                    value: 'Long',
                    name: 'Long'
                },
                {
                    value: 'BigDecimal',
                    name: 'BigDecimal'
                },
                {
                    value: 'LocalDate',
                    name: 'LocalDate'
                },
                {
                    value: 'DateTime',
                    name: 'DateTime'
                },
                {
                    value: 'Boolean',
                    name: 'Boolean'
                }
            ],
            default: 0
        }
    ];
    this.prompt(prompts, function (props) {
        if (props.fieldAdd) {
            var field = {fieldId: this.fieldId,
                fieldName: props.fieldName,
                fieldType: props.fieldType,
                fieldNameCapitalized: _s.capitalize(props.fieldName),
                fieldNameUnderscored: _s.underscored(props.fieldName),
                fieldNotNull: props.fieldNotNull}

            fieldNamesUnderscored.push(_s.underscored(props.fieldName));
            this.fields.push(field);
            if (props.fieldType == 'LocalDate') {
                this.fieldsContainLocalDate = true;
                this.fieldsContainCustomTime = true;
            }
            if (props.fieldType == 'BigDecimal') {
                this.fieldsContainBigDecimal = true;
            }
            if (props.fieldType == 'DateTime') {
                this.fieldsContainDateTime = true;
                this.fieldsContainCustomTime = true;
            }
            if (props.fieldNotNull) {
                this.fieldsContainNotNull = true;
            }
        }
        console.log(chalk.red('===========' + _s.capitalize(this.name) + '=============='));
        for (var id in this.fields) {
            console.log(chalk.red(this.fields[id].fieldName + ' (' + this.fields[id].fieldType + ')'));
        }
        if (props.fieldAdd) {
            this.askForFields();
        } else {
            cb();
        }
    }.bind(this));
};

EntityGenerator.prototype.files = function files() {
    
    this.entityClass = _s.capitalize(this.entityName);
    this.entityInstance = this.entityName.charAt(0).toLowerCase() + this.entityName.slice(1);

    var insight = this.insight();
    insight.track('generator', 'entity');
    insight.track('entity/fields', this.fields.length);

    this.template('entity/src/main/java/package/domain/_Entity.java',
        'entity/src/main/java/' + this.packageFolder + '/entity/' +    this.entityClass + '.java', this, {});

    this.template('dto/src/main/java/package/dto/_Dto.java',
        'dto/src/main/java/' + this.packageFolder + '/dto/' +    this.entityClass + 'Dto.java', this, {});

    this.template('repository/src/main/java/package/repository/_Repo.java',
        'repository/src/main/java/' + this.packageFolder + '/repository/I' +    this.entityClass + 'Repository.java', this, {});

    this.template('repository/src/integration/groovy/package/test/repository/_RepoSpec.groovy',
        'repository/src/integration/groovy/' + this.packageFolder + '/test/repository/' +    this.entityClass + 'RepositoryItSpec.groovy', this, {});
};

