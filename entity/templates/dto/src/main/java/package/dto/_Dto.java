package <%=packageName%>.dto;

import lombok.Data;

import java.io.Serializable;<% if (fieldsContainBigDecimal == true) { %>
import java.math.BigDecimal;<% }%><% if (fieldsContainLocalDate == true) { %>
import org.joda.time.LocalDate;<% } %><% if (fieldsContainDateTime == true) { %>
import org.joda.time.DateTime;<% } %>


@Data
public class <%= entityClass %>Dto implements Serializable {

	private Long id;	

	<% for (fieldId in fields) { %>
	private <%= fields[fieldId].fieldType %> <%= fields[fieldId].fieldName %>;
	<% } %>
}
