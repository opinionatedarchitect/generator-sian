package <%=packageName%>.test.repository

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import <%=packageName%>.RepositoryMockApplication;
import <%=packageName%>.repository.I<%=entityClass%>Repository;
import <%=packageName%>.entity.<%=entityClass%>;

import spock.lang.Specification

@ContextConfiguration(classes = [RepositoryMockApplication.class])
class <%=entityClass%>RepositoryItSpec extends Specification {
	
	@Autowired I<%=entityClass%>Repository repo;
	
	def "make sure the mapping works"() {
		when:
			1 == 1;
		
		then:
			1 == 2;
	}
}
