package <%=packageName%>.repository;

import <%=packageName%>.entity.<%=entityClass%>;
import org.springframework.data.jpa.repository.JpaRepository;


public interface I<%=entityClass%>Repository extends JpaRepository<<%=entityClass%>, Long> {

}
