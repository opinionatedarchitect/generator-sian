'use strict';
var util = require('util'),
    path = require('path'),
    yeoman = require('yeoman-generator'),
    chalk = require('chalk'),
    _s = require('underscore.string'),
    shelljs = require('shelljs'),
    scriptBase = require('../script-base'),
    packagejs = require(__dirname + '/../package.json');


var sianGenerator = module.exports = function sianGenerator(args, options, config) {

    yeoman.generators.Base.apply(this, arguments);

    //this.on('end', function () {
    //    this.installDependencies({
    //        skipInstall: options['skip-install'],
    //        callback: this._injectDependencies.bind(this)
    //    });
    //});

    //this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(sianGenerator, yeoman.generators.Base);
util.inherits(sianGenerator, scriptBase);



sianGenerator.prototype.askFor = function askFor() {
    var cb = this.async();

    console.log(chalk.red('\n' +
        'TEK Systems....\n' +   
        '   Code Generator:\n' +
        '   Golden Architecture\n' +
        '****************************************\n\n\n' 
         ));

    console.log('\nWelcome to the sian Generator\n');
    var insight = this.insight();

    var prompts = [
        {
            type: 'input',
            name: 'baseName',
            validate: function (input) {
                if (/^([a-zA-Z0-9_]*)$/.test(input)) return true;
                return '!!!!!!Your application name cannot contain special characters or a blank space, using the default name instead';
            },
            message: '(1/XXX) What is the base name of your application?',
            default: 'sian'
        },
        {
            type: 'input',
            name: 'packageName',
            validate: function (input) {
                if (/^([a-z_]{1}[a-z0-9_]*(\.[a-z_]{1}[a-z0-9_]*)*)$/.test(input)) return true;
                return 'The package name you have provided is not a valid Java package name.';
            },
            message: '(2/XXX) What is your default Java package name?',
            default: 'com.mycompany.myapp'
        },
        {
            type: 'list',
            name: 'javaVersion',
            message: '(3/XXX) Do you want to use Java 8?',
            choices: [
                {
                    value: '8',
                    name: 'Yes (use Java 8)'
                },
                {
                    value: '7',
                    name: 'No (use Java 7)'
                }
            ],
            default: 0
        },
        {
            type: 'list',
            name: 'buildTool',
            message: '(11/13) Would you like to use Maven or Gradle for building the backend?',
            choices: [
                {
                    value: 'maven',
                    name: 'Maven (recommended)'
                },
                {
                    value: 'gradle',
                    name: 'Gradle'
                }
            ],
            default: 'maven'
        }
    ];

    this.baseName = this.config.get('baseName');
    this.javaVersion = this.config.get('javaVersion');
    this.packageName = this.config.get('packageName');

    this.packageEntityName = this.packageName + '.entity';

    this.buildTool = this.config.get('buildTool');

    

    if (this.baseName != null ) {

        console.log(chalk.green('This is an existing project, using the configuration from your .yo-rc.json file \n' +
            'to re-generate the project...\n'));

        cb();
    } else {
        this.prompt(prompts, function (props) {
            if (props.insight !== undefined) {
                insight.optOut = !props.insight;
            }
            this.baseName = props.baseName;
            this.javaVersion = props.javaVersion;
            this.packageName = props.packageName;
            this.buildTool   = props.buildTool;
            
            cb();
        }.bind(this));
    }
};

sianGenerator.prototype.app = function app() {
    var insight = this.insight();
    insight.track('generator', 'app');
    
    this.angularAppName = _s.camelize(_s.slugify(this.baseName)) + 'App';

    this.config.set('baseName', this.baseName);
    this.config.set('javaVersion', this.javaVersion);
    this.config.set('basePackage Name', this.packageName);
    this.config.set('buildTool', this.buildTool);

    console.log(chalk.green("buildTool: " + this.buildTool + "\n"));

    var packageFolder = this.packageName.replace(/\./g, '/');
    var javaDir = 'src/main/java/' + packageFolder + '/';
    var unitTestDir = 'src/test/groovy/' + packageFolder + '/test/';
    var integrationTestDir = 'src/integration/groovy/' + packageFolder + '/';
    var integrationDir = 'src/integration/groovy/' + packageFolder + '/';
    var resourceDir = 'src/main/resources/';

    this.template('_README.md', 'README.md', this, {});
    this.copy('gitignore', '.gitignore');
    this.copy('gitattributes', '.gitattributes');

    switch (this.buildTool) {
        case 'gradle':
            this.template('_build.gradle', 'build.gradle', this, {});
            this.template('_gradle.properties', 'gradle.properties', this, {});
            
            this.copy('gradlew', 'gradlew');
            this.copy('gradlew.bat', 'gradlew.bat');
            this.copy('gradle/wrapper/gradle-wrapper.jar', 'gradle/wrapper/gradle-wrapper.jar');
            this.copy('gradle/wrapper/gradle-wrapper.properties', 'gradle/wrapper/gradle-wrapper.properties');
            break;
        case 'maven':
        default :
            this.template('_pom.xml', 'pom.xml', null, { 'interpolate': /<%=([\s\S]+?)%>/g });
    } 

    this.template('_settings.gradle', 'settings.gradle', this, {});

    this.template('app/_build.gradle', 'app/build.gradle', this, {});
    this.template('app/src/main/java/package/_Application.java', 'app/' + javaDir + '/Application.java', this, {});
    this.template('app/src/main/resources/config/_application.yml', 'app/src/main/resources/config/application.yml', this, {});


	this.template('entity/_build.gradle', 'entity/build.gradle', this, {});
    this.template('entity/src/main/java/package/PLACEHOLDER', 'entity/' + javaDir + '/PLACEHOLDER', this, {});
    
    this.template('business/_build.gradle', 'business/build.gradle', this, {});
    this.template('business/src/main/java/package/PLACEHOLDER', 'business/' + javaDir + '/PLACEHOLDER', this, {});
    this.template('business/src/test/groovy/package/PLACEHOLDER', 'business/src/test/groovy/PLACEHOLDER', this, {});

    this.template('repository/_build.gradle', 'repository/build.gradle', this, {});
    this.template('repository/src/main/java/package/PLACEHOLDER', 'repository/' + javaDir + '/PLACEHOLDER', this, {});
    this.template('repository/src/integration/groovy/package/RepositoryMockApplication.groovy', 'repository/' + integrationDir + '/RepositoryMockApplication.groovy', this, {});

    this.template('facade/_build.gradle', 'facade/build.gradle', this, {});
    this.template('facade/src/main/java/package/facade/SianFacadeConfiguration.java', 'facade/' + javaDir + '/facade/SianFacadeConfiguration.java', this, {});
    this.template('facade/src/test/groovy/PLACEHOLDER', 'facade/src/test/groovy/PLACEHOLDER', this, {});

    this.template('dto/_build.gradle', 'dto/build.gradle', this, {});
    this.template('dto/src/main/java/package/dto/_HelloWorld.java', 'dto/' + javaDir + '/dto/HelloWorld.java', this, {});

    this.template('rest/_build.gradle', 'rest/build.gradle', this, {});
    this.template('rest/src/main/java/package/rest/_IHelloWorldRest.java', 'rest/' + javaDir + '/rest/IHelloWorldRest.java', this, {});
    this.template('rest/src/main/java/package/rest/impl/_HelloWorldRest.java', 'rest/' + javaDir + '/rest/impl/HelloWorldRest.java', this, {});
    this.template('rest/src/test/groovy/package/test/rest/_HelloWorldRestUnitSpec.groovy', 'rest/' + unitTestDir + '/rest/HelloWorldRestUnitSpec.groovy', this, {});
    this.template('rest/src/integration/groovy/package/_MockRestApplication.groovy', 'rest/' + integrationTestDir + '/MockRestApplication.groovy', this, {});
    this.template('rest/src/integration/groovy/package/it/rest/_HelloWorldRestItSpec.groovy', 'rest/' + integrationTestDir + '/it/rest/HelloWorldRestItSpec.groovy', this, {});
    this.template('rest/src/main/java/package/config/apidoc/_SwaggerConfiguration.java', 'rest/' + javaDir + '/config/apidoc/SwaggerConfiguration.java', this, {});

    this.copy('rest/src/main/resources/templates/error.html', 'rest/src/main/resources/templates/error.html');
};

sianGenerator.prototype.projectfiles = function projectfiles() {
    this.copy('editorconfig', '.editorconfig');
    this.copy('jshintrc', '.jshintrc');
};

function removefile(file) {
    console.log('Remove the file - ' + file)
    if (shelljs.test('-f', file)) {
        shelljs.rm(file);
    }

}

function removefolder(folder) {
    console.log('Remove the folder - ' + folder)
    if (shelljs.test('-d', folder)) {
        shelljs.rm("-rf", folder);
    }
}

sianGenerator.prototype._injectDependencies = function _injectDependencies() {
//    if (this.options['skip-install']) {
//        this.log(
//            'After running `npm install & bower install`, inject your front end dependencies' +
//            '\ninto your source code by running:' +
//            '\n' +
//            '\n' + chalk.yellow.bold('grunt wiredep')
//        );
//    } else {
//        switch (this.frontendBuilder) {
//            case 'gulp':
//                this.spawnCommand('gulp', ['wiredep:test', 'wiredep:app']);
//                break;
//            case 'grunt':
//            default:
//                this.spawnCommand('grunt', ['wiredep']);
//        }
//    }
};
