package <%=packageName%>.test.rest

import <%=packageName%>.dto.HelloWorld;
import <%=packageName%>.rest.IHelloWorldRest;
import <%=packageName%>.rest.impl.HelloWorldRest;

import spock.lang.Specification;

class HelloWorldRestUnitSpec extends Specification {

	IHelloWorldRest helloWorldRest;
	
	def setup() {
		this.helloWorldRest = new HelloWorldRest();
	}
	
	
	def "simple hello"() {
		when:
			def helloWorld = this.helloWorldRest.greeting("World");
			
		then:
			"Hello, World!" == helloWorld.content;
	}
}
