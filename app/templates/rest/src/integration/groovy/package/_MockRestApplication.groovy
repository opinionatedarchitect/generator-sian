package <%=packageName%>

import org.dozer.DozerBeanMapper
import org.dozer.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod;

/**
 * Main class for the application boot.
 */
@ComponentScan
@EnableAutoConfiguration
public class MockRestApplication {
	
	/**
	 * Main method to start the application.
	 *
	 * @param args - No args required.
	 */
	public static void main(String[] args) {
		SpringApplication.run(MockRestApplication.class, args);
	}
}

