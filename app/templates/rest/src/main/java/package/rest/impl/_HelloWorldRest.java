package <%=packageName%>.rest.impl;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;

import <%=packageName%>.dto.HelloWorld;
import <%=packageName%>.rest.IHelloWorldRest;

/**
 * Super basic hello world class for the poc.
 */
@RestController
@RequestMapping("/api")
public class HelloWorldRest implements IHelloWorldRest {
	
    private AtomicLong atomicLong = new AtomicLong();
	
    @Override
    @RequestMapping(value="/helloworld",
    	method=RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HelloWorld> greeting(@RequestParam(value="name", required=false, defaultValue="World") final String name) {
        return new ResponseEntity<>(new HelloWorld(atomicLong.incrementAndGet(), String.format("Hello, %s!", name)), HttpStatus.OK);
    }
}

