package <%=packageName%>.rest;

import <%=packageName%>.dto.HelloWorld;
import org.springframework.http.ResponseEntity;

/**
 * Super basic hello world interface for the poc.
 */
public interface IHelloWorldRest {

	/**
     * Simple method to test the rest architecture.
     * 
     * @param name - The name to append to the hello message. Default is 'world'
     * @return - The HelloWorld object.
     */
	ResponseEntity<HelloWorld> greeting(final String name);
}