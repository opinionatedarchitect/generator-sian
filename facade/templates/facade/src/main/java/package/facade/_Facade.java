package <%=packageName%>.facade; 

import java.util.List;

import <%=packageName%>.dto.<%=entityClass%>Dto; 

public interface I<%=facadeClass%>Facade  {

	<%=entityClass%>Dto save(<%=entityClass%>Dto <%=entityInstance%>Dto);
	
	List<<%=entityClass%>Dto> findAll();
	
	<%=entityClass%>Dto findById(Long id);
}
