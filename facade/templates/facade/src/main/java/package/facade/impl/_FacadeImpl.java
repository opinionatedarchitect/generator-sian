package <%=packageName%>.facade.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import <%=packageName%>.entity.<%=entityClass%>;
import <%=packageName%>.dto.<%=entityClass%>Dto; 
import <%=packageName%>.business.I<%=businessClass%>Business;
import <%=packageName%>.facade.I<%=facadeClass%>Facade;

@Service
public class <%=facadeClass%>Facade implements I<%=facadeClass%>Facade {
	
	@Autowired I<%=businessClass%>Business <%=businessInstance%>Business;
	@Autowired Mapper mapper;
	
	@Override
	public <%=entityClass%>Dto save(<%=entityClass%>Dto <%=entityInstance%>Dto) {
		<%=entityClass%> <%=entityInstance%> = this.<%=businessInstance%>Business.save(this.mapper.map(<%=entityInstance%>Dto, <%=entityClass%>.class));
		return mapper.map(<%=entityInstance%>, <%=entityClass%>Dto.class);
	}
	
	@Override
	public List<<%=entityClass%>Dto> findAll() {
		List<<%=entityClass%>Dto> rlist = new ArrayList<<%=entityClass%>Dto>();
		for (<%=entityClass%> entity : this.<%=businessInstance%>Business.findAll()) {
			rlist.add(this.mapper.map(entity, <%=entityClass%>Dto.class));
		}
		return rlist;
	}
	
	@Override
	public <%=entityClass%>Dto findById(Long id) {
		return this.mapper.map(this.<%=businessInstance%>Business.findById(id), <%=entityClass%>Dto.class);
	}
}
