'use strict';
var util = require('util'),
        fs = require('fs'),
        path = require('path'),
        yeoman = require('yeoman-generator'),
        chalk = require('chalk'),
        _s = require('underscore.string'),
        shelljs = require('shelljs'),
        scriptBase = require('../script-base');


var FacadeGenerator = module.exports = function FacadeGenerator(args, options, config) {
    //yeoman.generators.NamedBase.apply(this, arguments);
    yeoman.generators.Base.apply(this, arguments);
    this.useConfigurationFile =false;

    this.baseName = this.config.get('baseName');
    this.packageName = this.config.get('basePackage Name');
    this.packageFolder = this.packageName.replace(/\./g, '/');
    this.javaVersion = this.config.get('javaVersion');
};

util.inherits(FacadeGenerator, yeoman.generators.Base);
util.inherits(FacadeGenerator, scriptBase);

FacadeGenerator.prototype.askForFields = function askForFields() {
    if (this.useConfigurationFile == true) {// don't prompt if data are imported from a file
        return;
    }
    var cb = this.async();
    this.fieldId++;
    console.log(chalk.green('Generating field #' + this.fieldId));
    var prompts = [
       {
            type: 'input',
            name: 'facadeName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id'/* && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1 */) return true;
                return 'Your facade name cannot contain special characters or use an already existing field name';
            },
           message: 'What is the name of your facade object?'
       },
       {
            type: 'input',
            name: 'businessName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id'/* && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1 */) return true;
                return 'Your business name cannot contain special characters or use an already existing field name';
            },
           message: 'What is the name of your business object?'
       },
        {
            type: 'input',
            name: 'entityName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id'/* && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1 */) return true;
                return 'Your entity name cannot contain special characters or use an already existing field name';
            },
           message: 'What is the name of your entity object?'
       }
    ];

    
    this.prompt(prompts, function (props) {
        this.facadeName = props.facadeName;
        this.businessName = props.businessName;
        this.entityName = props.entityName;
        cb();
    }.bind(this));
};



FacadeGenerator.prototype.files = function files() {
    //if (this.databaseType == "sql") {
    //    this.changelogDate = this.dateFormatForLiquibase();
    //}
    if (this.useConfigurationFile == false) { // store informations in a file for further use.
        this.data = {};
        this.data.relationships = this.relationships;
        this.data.fields = this.fields;
        this.data.fieldNamesUnderscored = this.fieldNamesUnderscored;
        this.data.fieldsContainOwnerManyToMany = this.fieldsContainOwnerManyToMany;
        this.data.fieldsContainOneToMany = this.fieldsContainOneToMany;
        this.data.fieldsContainLocalDate = this.fieldsContainLocalDate;
        this.data.fieldsContainCustomTime = this.fieldsContainCustomTime;
        this.data.fieldsContainBigDecimal = this.fieldsContainBigDecimal;
        this.data.fieldsContainDateTime = this.fieldsContainDateTime;
        this.data.changelogDate = this.changelogDate;
        this.filename = '.sian.' + this.facadeName + '.json';
        this.write(this.filename, JSON.stringify(this.data, null, 4));
    } else  {
        this.relationships = this.fileData.relationships;
        this.fields = this.fileData.fields;
        this.fieldNamesUnderscored = this.fileData.fieldNamesUnderscored;
        this.fieldsContainOwnerManyToMany = this.fileData.fieldsContainOwnerManyToMany;
        this.fieldsContainOneToMany = this.fileData.fieldsContainOneToMany;
        this.fieldsContainLocalDate = this.fileData.fieldsContainLocalDate;
        this.fieldsContainCustomTime = this.fileData.fieldsContainCustomTime;
        this.fieldsContainBigDecimal = this.fileData.fieldsContainBigDecimal;
        this.fieldsContainDateTime = this.fileData.fieldsContainDateTime;
        this.changelogDate = this.fileData.changelogDate;
    }
    this.facadeClass = _s.capitalize(this.facadeName);
    this.facadeInstance = this.facadeName.charAt(0).toLowerCase() + this.facadeName.slice(1);

    this.businessClass = _s.capitalize(this.businessName);
    this.businessInstance = this.businessName.charAt(0).toLowerCase() + this.businessName.slice(1);

    this.entityClass = _s.capitalize(this.entityName);
    this.entityInstance = this.entityName.charAt(0).toLowerCase() + this.entityName.slice(1);

    var insight = this.insight();
    insight.track('generator', 'facade');
    //insight.track('facade/fields', this.fields.length);
    //insight.track('facade/relationships', this.relationships.length);



    this.template('facade/src/main/java/package/facade/_Facade.java',
        'facade/src/main/java/' + this.packageFolder + '/facade/I' +    this.facadeClass + 'Facade.java', this, {});

    this.template('facade/src/main/java/package/facade/impl/_FacadeImpl.java',
        'facade/src/main/java/' + this.packageFolder + '/facade/impl/' +    this.facadeClass + 'Facade.java', this, {});
  
    this.template('facade/src/test/groovy/package/test/facade/_FacadeSpec.groovy',
        'facade/src/test/groovy/' + this.packageFolder + '/test/facade/' +    this.facadeClass + 'FacadeSpec.groovy', this, {});  
};

