'use strict';
var util = require('util'),
        fs = require('fs'),
        path = require('path'),
        yeoman = require('yeoman-generator'),
        chalk = require('chalk'),
        _s = require('underscore.string'),
        shelljs = require('shelljs'),
        scriptBase = require('../script-base');

 
var RestGenerator = module.exports = function RestGenerator(args, options, config) {
    //yeoman.generators.NamedBase.apply(this, arguments);
    yeoman.generators.Base.apply(this, arguments);
    this.useConfigurationFile =false;

    this.baseName = this.config.get('baseName');
    this.packageName = this.config.get('basePackage Name');
    this.packageFolder = this.packageName.replace(/\./g, '/');
    this.javaVersion = this.config.get('javaVersion');
};

util.inherits(RestGenerator, yeoman.generators.Base);
util.inherits(RestGenerator, scriptBase);

RestGenerator.prototype.askForFields = function askForFields() {
    if (this.useConfigurationFile == true) {// don't prompt if data are imported from a file
        return;
    }
    var cb = this.async();
    this.fieldId++;
    console.log(chalk.green('Generating field #' + this.fieldId));
    var prompts = [
       {
            type: 'input',
            name: 'restName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id'/* && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1 */) return true;
                return 'Your facade name cannot contain special characters or use an already existing field name';
            },
           message: 'What is the name of your rest object?'
       },
       {
            type: 'input',
            name: 'facadeName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id'/* && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1 */) return true;
                return 'Your facade name cannot contain special characters or use an already existing field name';
            },
           message: 'What is the name of your facade object?'
       },
        {
            type: 'input',
            name: 'entityName',
            validate: function (input) {
                if ((/^([a-zA-Z0-9_]*)$/.test(input)) && input != '' && input != 'id'/* && fieldNamesUnderscored.indexOf(_s.underscored(input)) == -1 */) return true;
                return 'Your entity name cannot contain special characters or use an already existing field name';
            },
           message: 'What is the name of your entity object?'
       }
    ];

    
    this.prompt(prompts, function (props) {
        this.restName   = props.restName;
        this.facadeName = props.facadeName;
        this.entityName = props.entityName;
        cb();
    }.bind(this));
};



RestGenerator.prototype.files = function files() {
    
    this.restClass = _s.capitalize(this.restName);
    this.restInstance = this.restName.charAt(0).toLowerCase() + this.restName.slice(1);

    this.facadeClass = _s.capitalize(this.facadeName);
    this.facadeInstance = this.facadeName.charAt(0).toLowerCase() + this.facadeName.slice(1);

    this.entityClass = _s.capitalize(this.entityName);
    this.entityInstance = this.entityName.charAt(0).toLowerCase() + this.entityName.slice(1);

    var insight = this.insight();
    insight.track('generator', 'facade');
    
    this.template('rest/src/main/java/package/rest/_Rest.java',
        'rest/src/main/java/' + this.packageFolder + '/rest/I' +    this.restClass + 'Rest.java', this, {});

    this.template('rest/src/main/java/package/rest/impl/_RestImpl.java',
        'rest/src/main/java/' + this.packageFolder + '/rest/impl/' +    this.restClass + 'Rest.java', this, {});
  
    //this.template('rest/src/test/groovy/package/test/rest/_RestSpec.java',
    //    'rest/src/test/groovy/' + this.packageFolder + '/test/rest/' +    this.restClass + 'RestSpec.java', this, {});  
};

