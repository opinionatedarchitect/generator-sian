
package <%=packageName%>.test.facade

import java.util.List;
import org.dozer.DozerBeanMapper;

import <%=packageName%>.business.I<%=businessClass%>Business;
import <%=packageName%>.entity.<%=entityClass%>; 
import <%=packageName%>.dto.<%=entityClass%>Dto; 

import <%=packageName%>.facade.I<%=facadeClass%>Facade;
import <%=packageName%>.facade.impl.<%=facadeClass%>Facade;


import spock.lang.Specification

class <%=facadeClass%>FacadeSpec extends Specification {
	
	I<%=facadeClass%>Facade <%=facadeInstance%>Facade;

	def setup() {
		this.<%=facadeInstance%>Facade = new <%=facadeClass%>Facade();
	}
	
	def "simple save"() {
		setup:
			def <%=entityInstance%> = [id:1L] as <%=entityClass%>;
			this.<%=facadeInstance%>Facade.@<%=businessInstance%>Business = [save: {<%=entityInstance%>} ] as I<%=businessClass%>Business;
			this.<%=facadeInstance%>Facade.@mapper = new DozerBeanMapper();
			
		when:
			def rdto = this.<%=facadeInstance%>Facade.save(new <%=entityClass%>Dto());
			
		then:
			rdto != null;
			rdto.id == 1L;
	}

	def "returning an empty list"() {
		setup:
			def <%=entityInstance%>s = [] as List<<%=entityClass%>>;
			this.<%=facadeInstance%>Facade.@<%=businessInstance%>Business = [findAll: {<%=entityInstance%>s} ] as I<%=businessClass%>Business;
			this.<%=facadeInstance%>Facade.@mapper = new DozerBeanMapper();
			
		when:
			def rlist = this.<%=facadeInstance%>Facade.findAll();
		
		then:
			rlist != null;
			rlist.isEmpty();
	}

	def "returning one in the list"() {
		setup:
			def <%=entityInstance%>s = [[id: 1L] as <%=entityClass%>] as List<<%=entityClass%>>;
			this.<%=facadeInstance%>Facade.@<%=businessInstance%>Business = [findAll: {<%=entityInstance%>s} ] as I<%=businessClass%>Business;
			this.<%=facadeInstance%>Facade.@mapper = new DozerBeanMapper();
			
		when:
			def rlist = this.<%=facadeInstance%>Facade.findAll();
		
		then:
			rlist != null;
			rlist.size() == 1;
	}

	def "find one by id"() {
		setup:
			this.<%=facadeInstance%>Facade.@<%=businessInstance%>Business = [findById: {[id: 1L] as <%=entityClass%>} ] as I<%=entityClass%>Business;
			this.<%=facadeInstance%>Facade.@mapper = new DozerBeanMapper();
			
		when:
			def rdto = this.<%=facadeInstance%>Facade.findById(1L);
		
		then:
			rdto != null;
			rdto.id == 1L;
	}
}
