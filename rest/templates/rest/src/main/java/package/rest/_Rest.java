package <%=packageName%>.rest; 

import java.util.List;

import <%=packageName%>.dto.<%=entityClass%>Dto; 
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

public interface I<%=restClass%>Rest  {

	ResponseEntity<<%= entityClass %>Dto> save(<%=entityClass%>Dto <%=entityInstance%>Dto);

	public List<<%= entityClass %>Dto> getAll();

	public ResponseEntity<<%= entityClass %>Dto> get(@PathVariable Long id);
}
