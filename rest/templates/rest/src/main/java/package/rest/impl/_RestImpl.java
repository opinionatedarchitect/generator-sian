package <%=packageName%>.rest.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import <%=packageName%>.dto.<%=entityClass%>Dto; 
import <%=packageName%>.facade.I<%=facadeClass%>Facade;
import <%=packageName%>.rest.I<%=restClass%>Rest;

@RestController
@RequestMapping("/api")
public class <%=restClass%>Rest implements I<%=restClass%>Rest {
	
	@Autowired I<%=facadeClass%>Facade <%=facadeInstance%>Facade;

	@RequestMapping(value = "/<%= entityInstance %>s",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<<%= entityClass %>Dto> save(@RequestBody <%= entityClass %>Dto <%= entityInstance %>Dto) {
        return new ResponseEntity<>(this.<%= facadeInstance %>Facade.save(<%= entityInstance %>Dto), HttpStatus.OK);
    }

     /**
     * GET  /<%= entityInstance %>s -> get all the <%= entityInstance %>s.
     */
    @RequestMapping(value = "/<%= entityInstance %>s",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<<%= entityClass %>Dto> getAll() {
        return this.<%=facadeInstance%>Facade.findAll();
    }

    /**
     * GET  /<%= entityInstance %>s/:id -> get the "id" <%= entityInstance %>.
     */
    @RequestMapping(value = "/<%= entityInstance %>s/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<<%= entityClass %>Dto> get(@PathVariable Long id) {
        <%= entityClass %>Dto <%= entityInstance %>Dto = this.<%= facadeInstance %>Facade.findById(id);
        if (<%= entityInstance %>Dto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(<%= entityInstance %>Dto, HttpStatus.OK);
    }
}
